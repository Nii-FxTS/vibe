#VPC CONFIG
resource "aws_vpc" "rehic" {
  cidr_block       = var.vpc_cidr
  instance_tenancy = "default"

  tags = {
    Name = "rehic"
  }
}

#Technical Team subnet config
resource "aws_subnet" "Tech-team" {
  vpc_id     = aws_vpc.rehic.id
  cidr_block = var.tsub

  tags = {
    Name = "Tech-team"
  }
}

#Fundamental Team subnet config
resource "aws_subnet" "Fund-team" {
  vpc_id     = aws_vpc.rehic.id
  cidr_block = "10.0.6.0/24"

  tags = {
    Name = "Fund-team"
  }
}

#Ushering Team subnet config
resource "aws_subnet" "ush-team" {
  vpc_id     = aws_vpc.rehic.id
  cidr_block = "10.0.8.0/24"

  tags = {
    Name = "ush-team"
  }
}



#Route Table config
resource "aws_route_table" "route-table-one" {
  vpc_id = aws_vpc.rehic.id
  tags = {
    Name = "route-table-one"
  }
}


#Route Table config
resource "aws_route_table" "route-table-two" {
  vpc_id = aws_vpc.rehic.id
  tags = {
    Name = "route-table-two"
  }
}

#Route table association Config
resource "aws_route_table_association" "socio-one" {
  subnet_id      = aws_subnet.Tech-team.id
  route_table_id = aws_route_table.route-table-one.id
}


#Route table association Config
resource "aws_route_table_association" "socio-two" {
  subnet_id      = aws_subnet.Fund-team.id
  route_table_id = aws_route_table.route-table-two.id
}


#Route table association Config
resource "aws_route_table_association" "socio-three" {
  subnet_id      = aws_subnet.ush-team.id
  route_table_id = aws_route_table.route-table-two.id
}


#internet gate config
resource "aws_internet_gateway" "IGW" {
  vpc_id = aws_vpc.rehic.id

  tags = {
    Name = "IGW"
  }
}


#route config
resource "aws_route" "route" {
  route_table_id            = aws_route_table.route-table-two.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                =aws_internet_gateway.IGW.id
}