#region variable config
variable "region" {
  default = "eu-west-2"
}

#Vpc cidr variable
variable "vpc_cidr" {
    default = "10.0.0.0/16"
}

#tech subnet cidr variable
variable "tsub" {
  default = "10.0.142.0/24"
}